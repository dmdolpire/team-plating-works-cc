package co.za.teamplatingworkscc;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class FindFragment extends Fragment implements OnMapReadyCallback {

    public FindFragment(){}
   GoogleMap googleMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_find, container, false);

        setHasOptionsMenu(true);

        //googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return rootView;

    }

    public void onMapReady(GoogleMap map) {
        LatLng sydney = new LatLng(-26.217977, 28.135734);

        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));

        map.addMarker(new MarkerOptions()
                .title("Team Plating Works cc")
                .snippet("64 Nasmith Ave, Jupiter Ext 5, Germiston, 1400, JHB, RSA ")
                .position(sydney));
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.nav, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {



            case R.id.action_navigate:
                Intent navigation = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q="+-26.217977+","+28.135734));
                startActivity(navigation);
                return true;


            default:
                return super.onOptionsItemSelected(item);


        }
    }


}
