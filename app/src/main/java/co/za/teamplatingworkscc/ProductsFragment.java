package co.za.teamplatingworkscc;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ProductsFragment extends ListFragment {

    public ProductsFragment(){}


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String[] values = new String[] { "Zinc", "Cadmium", "Copper", "Nickel", "Tin", "Brass", "EnviroPassTM", "MegaZincTM", "Mechanical Plating"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        switch( position )
        {
            case 0:  Intent newActivity = new Intent(v.getContext(), Zinc.class);
                startActivity(newActivity);
                break;
            case 1:  Intent newActivity1 = new Intent(v.getContext(), Cadmium.class);
                startActivity(newActivity1);
                break;
            case 2:  Intent newActivity2 = new Intent(v.getContext(), Copper.class);
                startActivity(newActivity2);
                break;
            case 3:  Intent newActivity3 = new Intent(v.getContext(), Nickel.class);
                startActivity(newActivity3);
            break;
            case 4:  Intent newActivity4 = new Intent(v.getContext(), Tin.class);
                startActivity(newActivity4);
                break;
            case 5:  Intent newActivity5 = new Intent(v.getContext(), Brass.class);
                startActivity(newActivity5);
                break;
            case 6:  Intent newActivity6 = new Intent(v.getContext(), EnvioPass.class);
                startActivity(newActivity6);
                break;
            case 7:  Intent newActivity7 = new Intent(v.getContext(), MegaZinc.class);
                startActivity(newActivity7);
                break;
            case 8:  Intent newActivity8 = new Intent(v.getContext(), Mechanical.class);
                startActivity(newActivity8);
                break;


        }
    }
}

