package co.za.teamplatingworkscc;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ContactFragment extends ListFragment {

    public ContactFragment(){}


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String[] values = new String[] { "Accounts Department: Tracy", "Production Department: Gary"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

            switch( position )
            {
                case 0:  Intent newActivity = new Intent(v.getContext(), Account.class);
                    startActivity(newActivity);
                    break;
                case 1:  Intent newActivity2 = new Intent(v.getContext(), Production.class);
                    startActivity(newActivity2);
                    break;

            }
        }
}

