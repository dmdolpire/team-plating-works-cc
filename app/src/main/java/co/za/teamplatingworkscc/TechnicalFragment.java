package co.za.teamplatingworkscc;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class TechnicalFragment extends ListFragment {

    public TechnicalFragment(){}


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String[] values = new String[] { "Rohs: Max Allowed PPM", "Environmental Corrosion", "Salt Spray Hours"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        switch( position )
        {
            case 0:  Intent newActivity = new Intent(v.getContext(), Rohs.class);
                startActivity(newActivity);
                break;
            case 1:  Intent newActivity2 = new Intent(v.getContext(), Corrosion.class);
                startActivity(newActivity2);
                break;
            case 2:  Intent newActivity3 = new Intent(v.getContext(), Salt.class);
                startActivity(newActivity3);
                break;


        }
    }
}

