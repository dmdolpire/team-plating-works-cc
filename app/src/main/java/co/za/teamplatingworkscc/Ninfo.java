package co.za.teamplatingworkscc;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class Ninfo extends Activity {

    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ninfo);

        setTitle("Back");

        ActionBar actionBar = getActionBar();

        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(true);

        mWebView = (WebView) findViewById(R.id.activity_main_webview);

        mWebView.loadUrl("http://teamplating.co.za/pages/products/nickel.php");

        final ProgressDialog progressDialog = new ProgressDialog(Ninfo.this);
        progressDialog.setMessage("Loading ...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        // Settings for WebView
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);


        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressDialog.hide();
            }

            ;

            @Override
            public void onReceivedError(WebView view,
                                        int errCode, String description, String failedUrl) {
                String html1 = "<html><body><br><br><div><h3>An error occurred.</h3><hr>";
                String html2 = "<p>Check your Internet connection!</p></div></body></html>";
                String mime = "text/html";
                String encoding = "utf-8";
                view.loadData(html1 + html2, mime, encoding);
            }
        });

    }
}